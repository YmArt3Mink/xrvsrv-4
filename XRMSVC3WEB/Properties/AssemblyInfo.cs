﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью указанного ниже 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("XRMSVC3WEB")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hewlett-Packard Company")]
[assembly: AssemblyProduct("XRMSVC3WEB")]
[assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Если для атрибута ComVisible задать значение FALSE, типы в этой сборке будут невидимыми 
// для компонентов COM. Если в этой сборке необходимо получить доступ к типу из 
// компонента COM, задайте для атрибута ComVisible этого типа значение TRUE.
[assembly: ComVisible(false)]

// Указанный ниже идентификатор GUID предназначен для идентификации библиотеки типов, если этот проект будет видимым для COM-объектов
[assembly: Guid("844b9a6d-ab49-4226-84a6-089bb0a05ce5")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер сборки
//      Номер редакции
//
// Можно указать все значения или установить для номеров редакции и сборки значение по умолчанию 
// с помощью символа "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
