﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

namespace XRMSVC3WEB
{
    #region Interfaces 

    [ServiceContract]
    public interface IBookService
    {
        [OperationContract(IsInitiating = true)]
        [WebInvoke(
            UriTemplate = "/hello",
            Method = "POST")]
        void SayHello(Student student);

        [OperationContract(IsOneWay = true)]
        [WebInvoke(
            UriTemplate = "/bye",
            Method = "POST")]
        void SayGoodbye();

        [OperationContract]
        [WebInvoke(
            UriTemplate = "/apply", 
            Method = "POST")]
        void ApplyChanges();
    
        [OperationContract]
        [WebGet(
            UriTemplate = "/{id}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Book Get(string id);
        
        [OperationContract]
        [WebInvoke(
            UriTemplate = "/add", 
            Method = "POST")]
        void Add(Book book);

        [OperationContract]
        [FaultContract(typeof(BookFaultModel))]
        [WebInvoke(
            UriTemplate = "/rent/{bookId}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            Method = "GET")]
        Book Rent(string bookId);

        [OperationContract]
        [FaultContract(typeof(BookFaultModel))]
        [WebInvoke(
            UriTemplate = "/", 
            Method = "POST")]
        void GiveBack(Book book);
    }

    [ServiceContract]
    public interface IBookServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnCallback();
    }

    #endregion

    #region Models

    [DataContract]
    public enum PublicationType
    {
        [EnumMember]
        Literature,

        [EnumMember]
        Magazine,

        [EnumMember]
        Journal,

        [EnumMember]
        Comicbook
    }

    [DataContract]
    public class Book 
        : IEquatable<Book>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public uint Year { get; set; }

        [DataMember]
        public PublicationType PublicationType { get; set; }

        public bool IsRented { get; set; }

        [DataMember]
        public DateTime? RentedSince { get; set; }

        public bool Equals(Book other)
        {
            return
                (Id == other.Id)
                || (Name.Equals(other.Name)
                    && Author.Equals(other.Author)
                    && Year == other.Year
                    && PublicationType == other.PublicationType);
        }
    }

    [DataContract]
    public class Student
        : IEquatable<Student>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public ICollection<Book> Books { get; set; }

        public bool Equals(Student other)
        {
            return
                (Id == other.Id) && FullName.Equals(other.FullName);
        }
    }

    [DataContract]
    public class BookFaultModel
    {
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

}
