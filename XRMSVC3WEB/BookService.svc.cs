﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace XRMSVC3WEB
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class BookService
        : IBookService, IServiceBehavior, IDisposable
    {
        private const int RENTING_LIMIT = 5;
        static readonly TimeSpan _rentingTimeLimit = new TimeSpan(31, 0, 0, 0, 0);

        private Student _student;
        private ICollection<Book> _booksCart
            = new List<Book>();
        private ICollection<Book> _books
            = new List<Book>();

        private IBookServiceCallback Callback
        {
            get
            {
                return
                    OperationContext.Current.GetCallbackChannel<IBookServiceCallback>();
            }
        }

        public BookService()
        {
            _books.Add(new Book
            {
                Id = 1,
                Name = "Name_1",
                Author = "Author_1",
                PublicationType = PublicationType.Literature,
                Year = 1998
            });
            _books.Add(new Book
            {
                Id = 2,
                Name = "Name_1",
                Author = "Author_2",
                PublicationType = PublicationType.Magazine,
                Year = 1986
            });
            _books.Add(new Book
            {
                Id = 1,
                Name = "Name_2",
                Author = "Author_1",
                PublicationType = PublicationType.Literature,
                Year = 2004
            });
        }

        [OperationBehavior(ReleaseInstanceMode = ReleaseInstanceMode.BeforeCall)]
        public void SayHello(Student student)
        {
            Console.WriteLine("Welcome, " + student.FullName);
            _student = student;
            _booksCart = _student.Books;
        }

        public void ApplyChanges()
        {
            UpdateStudentCart();

            bool hasOutdatedBooks = HasOutdatedBooks(_booksCart);
            if (hasOutdatedBooks)
            {
                Console.WriteLine("WTF do you think you're doing tho?\nWhen do you give your books back");
                Callback.OnCallback();
            }

            Console.WriteLine("Applied!");
        }

        [OperationBehavior(ReleaseInstanceMode = ReleaseInstanceMode.AfterCall)]
        public void SayGoodbye()
        {
            Console.WriteLine("Bye-bye!");
            this.Dispose();
        }

        public Book Get(string id)
        {
            Console.WriteLine("Get (by Id)");
            return _books.First(i => id.Equals(i.Id.ToString()));
        }

        public void Add(Book book)
        {
            Console.WriteLine("Add");
            _books.Add(book);
        }

        public Book Rent(string bookId)
        {
            Console.WriteLine("Take");
            if (_booksCart.Count > RENTING_LIMIT)
            {
                Console.WriteLine("Um sorry, pal! It's better to give smth back at first");
                return null;
            }

            var renteeBook = _books.First(i => bookId.Equals(i.Id.ToString()) && !i.IsRented);
            if (renteeBook == null)
            {
                var faultModel = new BookFaultModel { Message = "You have chosen an unexisted book" };
                throw new FaultException<BookFaultModel>(faultModel);
            }
            _booksCart.Add(renteeBook);

            return renteeBook;
        }

        public void GiveBack(Book book)
        {
            Console.WriteLine("GiveBack");
            var renteeBook = _books.First(i => i.Equals(book) && i.IsRented);
            if (renteeBook == null)
            {
                var faultModel = new BookFaultModel { Message = "You have chosen an already rented book" };
                throw new FaultException<BookFaultModel>(faultModel);
            }
            _booksCart.Remove(renteeBook);
        }

        private void UpdateStudentCart()
        {
            _booksCart
                .Where(book => !_student.Books.Contains(book))
                .ToList()
                .ForEach(book =>
                {
                    book.IsRented = true;
                    book.RentedSince = DateTime.Now;
                });
            _student.Books
                .Where(book => !_booksCart.Contains(book))
                .ToList()
                .ForEach(book =>
                {
                    book.IsRented = false;
                    book.RentedSince = null;
                });

            _student.Books = _booksCart;
        }

        private bool HasOutdatedBooks(ICollection<Book> books)
        {
            return
                books.Any(book => (DateTime.Now - book.RentedSince) > _rentingTimeLimit);
        }

        public void Dispose()
        {
            _student = null;
            _books = null;
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler handler = new BookErrorHandler();
            foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
            {
                var dispatcher = (ChannelDispatcher)channelDispatcherBase;
                dispatcher.ErrorHandlers.Add(handler);
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }
    }

    public class BookErrorHandler : IErrorHandler
    {
        public bool HandleError(Exception error)
        {
            Debug.WriteLine("HandleError");
            return true;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            Debug.WriteLine("ProvideFault");
        }
    }
}
